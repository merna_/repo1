import React from 'react';
import './carSider.css';
import slick from 'slick-carousel/slick/slick.js';
import $ from 'jquery'
class CarSlider extends React.Component{
  state={
    upics : [
      "https://imgd.aeplcdn.com/0x0/n/cw/ec/41197/hyundai-verna-right-front-three-quarter7.jpeg",
      "https://cdn1.droom.in//uploads/category/hyundai/20200403051659000000-7653951643646162945.jpg",
      "https://image.shutterstock.com/image-photo/image-front-sports-car-scene-260nw-566330083.jpg"
    ]
  }
    componentDidMount(){
        $('.slider-for')
          .slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            adaptiveHeight: true,
            asNavFor: '.slider-nav'
          })
         
        $('.slider-nav').slick({
          slidesToShow: 6,
          slidesToScroll: 1,
          asNavFor: '.slider-for',
          dots: false,
          centerMode: false,
          focusOnSelect: true,
          variableWidth: true,
           prevArrow: '<span class="slick-prev fa fa-chevron-left"></span>',
           nextArrow: '<span class="slick-next fa fa-chevron-right"></span>'
        });
      
      }
     
       /*download = e => {
        fetch('https://upload.wikimedia.org/wikipedia/en/6/6b/Hello_Web_Series_%28Wordmark%29_Logo.png', {
          method: "GET",
          headers: {}
        }).then(response => {
            response.arrayBuffer().then(function(buffer) {
              const url = window.URL.createObjectURL(new Blob([buffer]));
              const link = document.createElement("a");
              link.href = url;
              link.setAttribute("download", "image.jpg"); //or any other extension
              document.body.appendChild(link);
              link.click();
            });
          })
          fetch('https://upload.wikimedia.org/wikipedia/commons/d/d5/Tulips_10_IMG.jpg', {
            method: "GET",
            headers: {}
          }).then(response => {
              response.arrayBuffer().then(function(buffer) {
                const url = window.URL.createObjectURL(new Blob([buffer]));
                const link = document.createElement("a");
                link.href = url;
                link.setAttribute("download", "image.jpg"); //or any other extension
                document.body.appendChild(link);
                link.click();
              });
            })
      };*/
      
      download=(item)=> {
        var fileName = item.split(/(\\|\/)/g).pop();
      
        var image = new Image();
        image.crossOrigin = "anonymous";
        image.src = item;
        image.onload = function() {
        
          // use canvas to load image
          var canvas = document.createElement('canvas');
          canvas.width = this.naturalWidth;
          canvas.height = this.naturalHeight;
          canvas.getContext('2d').drawImage(this, 0, 0);
          
          // grab the blob url
          var blob;
          if (image.src.indexOf(".jpg") > -1) {
            blob = canvas.toDataURL("image/jpeg");
          } else if (image.src.indexOf(".png") > -1) {
            blob = canvas.toDataURL("image/png");
          } else if (image.src.indexOf(".gif") > -1) {
            blob = canvas.toDataURL("image/gif");
          } else {
            blob = canvas.toDataURL("image/png");
          }
      
          // create link, set href to blob
          var a = document.createElement('a');
          a.title = fileName;
          a.href = blob;
          a.style.display = 'none';
          a.setAttribute("download", fileName);
          a.setAttribute("target", "_blank");
          document.body.appendChild(a);
          
          // click item
          a.click();
        }
      }
       downloadAll(item) {
        for (var i in this.state.upics) {
          this.download(this.state.upics[i]);
        }
      }
    render(){
    return(
        <div className="carSlider">   
          <div className="photos">
            <ul>
              
              <li><h5>Photos</h5></li>
              <a
        onClick={()=>this.downloadAll()}
      >
               <li><i className="fa fa-arrow-circle-down"  ></i></li>
               </a>
             </ul>
          </div>
      							<div class="slider-for">
								<a href="https://imgd.aeplcdn.com/0x0/n/cw/ec/41197/hyundai-verna-right-front-three-quarter7.jpeg" class="item-slick"><img src="https://imgd.aeplcdn.com/0x0/n/cw/ec/41197/hyundai-verna-right-front-three-quarter7.jpeg" alt="Alt"/></a>
								<a href="https://cdn1.droom.in//uploads/category/hyundai/20200403051659000000-7653951643646162945.jpg" class="item-slick"><img src="https://cdn1.droom.in//uploads/category/hyundai/20200403051659000000-7653951643646162945.jpg" alt="Alt"/></a>
								<a href="https://image.shutterstock.com/image-photo/image-front-sports-car-scene-260nw-566330083.jpg" class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458571-340.jpg" alt="Alt"/></a>
								<a href="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-593.jpg" class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-593.jpg" alt="Alt"/></a>
							
              	<a href="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-74.jpg" class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-74.jpg" alt="Alt"/></a>
								<a href="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-30.jpg" class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-30.jpg" alt="Alt"/></a>
								<a href="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-383.jpg" class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-383.jpg" alt="Alt"/></a>
								<a href="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-828.jpg" class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-828.jpg" alt="Alt"/></a>
							
              </div>
							<div class="slider-nav">
								<div class="item-slick"><img src="https://imgd.aeplcdn.com/0x0/n/cw/ec/41197/hyundai-verna-right-front-three-quarter7.jpeg" alt="Alt"/></div>
								<div class="item-slick"><img src="https://cdn1.droom.in//uploads/category/hyundai/20200403051659000000-7653951643646162945.jpg" alt="Alt"/></div>
								<div class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458571-340.jpg" alt="Alt"/></div>
								<div class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-593.jpg" alt="Alt"/></div>
                <div class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-74.jpg" alt="Alt"/></div>
								<div class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-30.jpg" alt="Alt"/></div>
								<div class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-383.jpg" alt="Alt"/></div>
								<div class="item-slick"><img src="https://cdn.syarah.com/online/posts/40636/0x683/orignal-1605458581-828.jpg" alt="Alt"/></div>
							
                
              </div>
      
 
        </div>
    )
}
}
export default CarSlider