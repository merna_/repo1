import React from "react";
import "./carInfo.css";
import copy from "copy-to-clipboard";
import { toast } from "react-toastify";

class CarInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      textToCopy: "2019 MercedesBenz C180",
      text2: "2G1WT57N591216081",
      customId: "custom-id-yes",
      customId2: "custom-id-yes2",
      color: "#707070",
      price: 20000,
    };
    this.Copytext = this.Copytext.bind(this);
  }
  Copytext(name) {
    copy(name);
    toast("Car model copied", {
      toastId: this.state.customId,
    });
  }
  Copytext2(name) {
    copy(name);
    toast("Car Data copied", {
      toastId: this.state.customId2,
    });
  }
  render() {
    return (
      <div className="carInfo">
        <div className="carName row">
          <p>{this.state.textToCopy}</p>
          <img
            src="Mask Group 120.svg"
            className="m"
            onClick={() => this.Copytext(this.state.textToCopy)}
          />
        </div>
        <div className="carData row">
          <p>{this.state.text2}</p>
          <img
            src="Mask Group 120.svg"
            onClick={() => this.Copytext2(this.state.text2)}
          />
        </div>
        <div className="info container">
          <div className="row">
            <div className="col-md-2 col-sm-3 col-4">
              <div className="item">
                <h6>6527213</h6>
                <p>LOT#</p>
              </div>
            </div>
            <div className="col-md-2 col-sm-3 col-4">
              <div className="item s">
                <h6 className="row">
                  Dark Grey
                  <div style={{ backgroundColor: this.state.color }}></div>
                </h6>
                <p>Color</p>
              </div>
            </div>
            <div className="col-md-2 col-sm-3 col-4">
              <div className="item">
                <h6>
                  {this.state.price.toLocaleString(navigator.language, {
                    minimumFractionDigits: 0,
                  })}
                </h6>
                <p>Miles</p>
              </div>
            </div>
          </div>
        </div>

        <div className=" container">
          <div className="row">
            <div className="col-md-2 col-sm-2 col-4">
              <div className=" icons">
                <p>9</p> <img src="Mask Group 1172.svg" />
              </div>
            </div>
            <div className="col-md-2 col-sm-2 col-4">
              <div className=" icons">
                <img src="Group 8.svg" />{" "}
              </div>
            </div>
            <div className="col-md-2 col-sm-2 col-4">
              <div className=" icons">
                <p>9</p> <img src="Mask Group 1147.svg" />
              </div>
            </div>
            <div className="col-md-2 col-sm-2 col-4">
              <div className=" icons ">
                <p>9</p>
                <img src="Mask Group 122.svg" />
              </div>
            </div>
            <div className="col-md-2 col-sm-2 col-4">
              <div className=" icons">
                <img src="Group 195.svg" />
              </div>
            </div>
            <div className="col-md-2 col-sm-2 col-4">
              <div className=" icons">
                <img src="Group 7.svg" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default CarInfo;
