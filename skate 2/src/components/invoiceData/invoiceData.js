import React from "react";
import BillingFrom from "./billingFrom/billingFrom";
import BillingInfo from "./billingInfo/billingInfo";
import "./invoiceData.css";
import InvoiceFooter from "./invoiceFooter/invoiceFooter";
import InvoiceTable from "./invoiceTable/invoiceTable";

class InvoiceData extends React.Component {
  state = {
    items: [],
  };
  componentDidMount() {
    window.scrollTo(0, 0);
    fetch("s.json")
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          items: result,
        });
      });
  }
  render() {
    const { items } = this.state;
    return (
      <div className="invoiceData">
        <div className="container ">
          <br />
          <div className="invoiceData2">
            <div className="inviceDataHeader">
              <div className="logo"></div>
            </div>
            <div className="data">
                  <BillingInfo />
                  <InvoiceTable items={items}/>
                  <BillingFrom />
            </div>
            <InvoiceFooter />
          </div>
        </div>
      </div>
    );
  }
}
export default InvoiceData;
