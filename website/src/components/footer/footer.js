import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/images/logo.png";
import "./footer.css";

export const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="brand">
            <img src={logo} alt="logo" />
          </div>
          <div className="links">
              <ul>
              <li><Link to="/company">Company</Link></li>
              <li><Link to="/company">Contact</Link></li>
              <li><Link to="/company">About</Link></li>
              </ul>
          </div>
        </div>
      </div>
    </footer>
  );
};
