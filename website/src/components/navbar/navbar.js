import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/images/logo.png";

import "./navbar.css";

export const Navbar = () => {
  return (
    <nav className="navbar">
        <div className="container">
      <div className="logo">
        <img src={logo} alt="logo" />
      </div>
      <div className="links">
        <ul>
          <li className="nav-link">
            <Link to="/contact">Contact</Link>
          </li>
          <li className="nav-link">
              <Link to='/about'>About</Link>
          </li>
        </ul>
      </div>
      </div>
      </nav>
  );
};
