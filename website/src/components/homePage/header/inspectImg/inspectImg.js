import React from 'react';
import inspectImg from '../../../../assets/images/inspectImg.png';
import './inspectImg.css';

export const InspectImg = () => {
    return (
        <div className="inspectImg">
            <img src={inspectImg} alt="inspectImg"/>
        </div>
    );
};

 