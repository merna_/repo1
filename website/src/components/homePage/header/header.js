import React from 'react';
import { InspectPaid } from './inspectPaid/inspectPaid';
import {InspectImg} from './inspectImg/inspectImg';
import './header.css';

export const Header = () => {
    return (
        <div className="header">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6">
                        <InspectPaid/>
                    </div>
                    <div className="col-lg-6">
                        <InspectImg/>
                    </div>
                </div>
            </div>
        </div>
    );
};

 