import React from 'react';
import { Footer } from '../footer/footer';
import { Navbar } from '../navbar/navbar';
import { Header } from './header/header';

export const HomePage = () => {
    return (
       <>
        <Navbar/>
        <Header/>
        <Footer/>
       </>
    );
};

 